import 'dart:convert';
import 'package:http/http.dart' as http;

import '../cities/city.dart';
import 'weather.dart';

/// Fetches the weather data for a given city.
Future<Weather?> fetchWeatherData(City city) async {
  // Prepare url with query parameters.
  final url = Uri.https(
    'api.open-meteo.com',
    '/v1/forecast',
    {
      'latitude': city.lat.toString(),
      'longitude': city.lon.toString(),
      'current_weather': 'true',
      'hourly':
          'temperature_2m,rain,apparent_temperature,windspeed_10m,snowfall',
    },
  );
  final response = await http.get(url);
  final jsonObject = json.decode(response.body);

  // All weather data is structured an hourly lists.
  final hourly = jsonObject['hourly'] as Map<String, dynamic>;
  // First I map the list of <key,date string> into something I can easily filter.
  final timeEntries = (hourly['time'] as List).asMap().entries;
  final timeList = timeEntries
      .map(
        (e) => _Time(
          key: e.key,
          date: DateTime.parse(e.value),
        ),
      )
      .toList();

  // I create a date object of now with the same format as the hourly dates.
  final now = DateTime.now();
  final date = DateTime(now.year, now.month, now.day, now.hour);
  // I search for the date that matches now to get the key.
  final result = timeList.where((e) => e.date.isAtSameMomentAs(date)).toList();
  if (result.isEmpty) {
    return null;
  }

  final temperature = hourly['temperature_2m'][result.first.key];
  final rain = hourly['rain'][result.first.key];
  final apparentTemperature = hourly['apparent_temperature'][result.first.key];
  final windSpeed = hourly['windspeed_10m'][result.first.key];
  final snowfall = hourly['snowfall'][result.first.key];

  return Weather(
    temp: temperature,
    apparentTemperature: apparentTemperature,
    rain: rain,
    snowfall: snowfall,
    windSpeed: windSpeed,
  );
}

class _Time {
  final int key;
  final DateTime date;

  const _Time({required this.date, required this.key});
}
