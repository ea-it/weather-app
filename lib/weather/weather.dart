class Weather {
  final double temp;
  final double apparentTemperature;
  final double rain;
  final double snowfall;
  final double windSpeed;

  const Weather({
    required this.temp,
    required this.apparentTemperature,
    required this.rain,
    required this.snowfall,
    required this.windSpeed,
  });
}
