import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../state_controller.dart';
import 'weather.dart';

class WeatherWidget extends StatelessWidget {
  const WeatherWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      final weather = StateController.to.weather.value;
      if (weather == null) {
        return const SizedBox();
      }
      return Padding(
        padding: const EdgeInsets.only(top: 16),
        child: Column(
          children: [
            Text(
              '${weather.temp.toString()} °C',
              style: const TextStyle(fontSize: 20),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Image(
                image: AssetImage(_imageForWeather(weather)),
                height: 200,
                width: 200,
              ),
            ),
          ],
        ),
      );
    });
  }
}

String _imageForWeather(Weather weather) {
  if (weather.rain > 3) {
    return 'assets/images/sunny.png';
  } else if (weather.snowfall > 0) {
    return 'assets/images/snow.png';
  }
  return 'assets/images/sunny.png';
}
