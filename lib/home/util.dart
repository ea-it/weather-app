import 'greetings.dart';

Greetings prepareGreeting({required DateTime now}) {
  final todayMorning = DateTime(now.year, now.month, now.day, 2, 59, 0);
  final todayNoon = DateTime(now.year, now.month, now.day, 11, 59, 0);
  final todayEvening = DateTime(now.year, now.month, now.day, 17, 59, 0);
  final tomorrowMorning = DateTime(
    now.year,
    now.month,
    now.day + 1,
    3,
    0,
    0,
  );

  if (now.isAfter(todayMorning) && now.isBefore(todayNoon)) {
    return Greetings.morning;
  }

  if (now.isAfter(todayEvening) && now.isBefore(tomorrowMorning)) {
    return Greetings.evening;
  }

  return Greetings.noon;
}
