enum Greetings {
  morning,
  noon,
  evening,
}

extension GreetingsExt on Greetings {
  String get text {
    switch (this) {
      case Greetings.morning:
        return 'Good morning!';
      case Greetings.noon:
        return 'Hello!';
      case Greetings.evening:
        return 'Good evening!';
    }
  }
}
