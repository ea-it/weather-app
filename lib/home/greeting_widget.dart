import 'package:flutter/material.dart';
import 'package:weather_app/home/greetings.dart';

import 'util.dart';

class GreetingWidget extends StatelessWidget {
  const GreetingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return _buildWidget(prepareGreeting(now: DateTime.now()));
  }
}

Widget _buildWidget(Greetings greeting) {
  return Padding(
    padding: const EdgeInsets.only(top: 24),
    child: Text(
      greeting.text,
      style: const TextStyle(fontSize: 30),
    ),
  );
}
