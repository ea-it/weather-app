import 'package:country_flags/country_flags.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/weather/fetch_weather_data.dart';

import '../cities/city.dart';
import '../state_controller.dart';
import '../weather/weather_widget.dart';
import 'greeting_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final stateController = StateController.to;

    final currentCity = stateController.currentCity;
    if (currentCity.value == null) {
      _loadCity();
    }

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              _buildProfileImage(),
              const GreetingWidget(),
              Obx(() => _buildCityWidget(stateController.currentCity.value)),
              const WeatherWidget(),
            ],
          ),
        ],
      ),
    );
  }
}

Widget _buildProfileImage() => Padding(
      padding: const EdgeInsets.only(top: 64),
      child: Container(
        width: 150,
        height: 150,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: AssetImage('assets/images/profile.jpeg'),
            fit: BoxFit.fill,
          ),
        ),
      ),
    );

Widget _buildCityWidget(City? city) {
  if (city == null) {
    return const Text(
      'Please select city',
      style: TextStyle(fontSize: 30),
    );
  }
  return Padding(
    padding: const EdgeInsets.only(top: 50),
    child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 16),
          child: Text(
            city.name,
            style: const TextStyle(fontSize: 30),
          ),
        ),
        CountryFlag.fromCountryCode(
          city.countryCode,
          height: 30,
          width: 30,
          borderRadius: 4,
        ),
      ],
    ),
  );
}

/// Checks persistent storage for a saved city, loads it
/// and puts it into the state controller.
/// Also fetches new weather data.
void _loadCity() {
  SharedPreferences.getInstance().then((prefs) async {
    final stateController = StateController.to;
    final cityName = prefs.getString('city');

    final result = stateController.cityList
        .where((city) => city.name == cityName)
        .toList();
    if (result.isNotEmpty) {
      stateController.setCity(result.first);
      final weather = await fetchWeatherData(result.first);
      stateController.setWeather(weather);
    }
  }).catchError((error) {
    debugPrint(error);
  });
}
