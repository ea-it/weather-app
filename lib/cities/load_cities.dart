import 'dart:convert';

import 'package:flutter/services.dart';

import 'city.dart';

Future<List<City>> loadCities() async {
  final String fileContent =
      await rootBundle.loadString('assets/data/cities.json');
  final List data = json.decode(fileContent);
  final cities = data.map((e) => City.fromJson(e)).toList();
  return cities;
}
