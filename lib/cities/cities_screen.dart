import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../state_controller.dart';
import '../weather/fetch_weather_data.dart';
import 'cities_list_widget.dart';
import 'city.dart';

class CitiesScreen extends HookWidget {
  const CitiesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final selectedCity = useState<City?>(null);

    final textController = TextEditingController(
      text: StateController.to.filter.value,
    );

    return Scaffold(
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 24),
            child: Text(
              'Select a city',
              style: TextStyle(fontSize: 24),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8),
            child: TextField(
              controller: textController,
              onChanged: (value) => StateController.to.setFilter(value),
            ),
          ),
          Expanded(
            child: CitiesList(selectedCity: selectedCity),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 8),
            child: OutlinedButton(
                onPressed: selectedCity.value == null
                    ? _buildWarning
                    : () => _onSetButtonPressed(selectedCity),
                child: const Text('set')),
          ),
        ],
      ),
    );
  }
}

Future<void> _saveCityToSharedPrefs(String city) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString('city', city);
  return;
}

void _buildWarning() => Get.snackbar(
      'No city selected',
      'To set a new city tap one above and press the button again.',
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.orange,
    );

void _onSetButtonPressed(ValueNotifier<City?> selectedCity) async {
  final city = selectedCity.value!;
  final weather = await fetchWeatherData(city);
  StateController.to.setWeather(weather);
  StateController.to.setCity(city);
  await _saveCityToSharedPrefs(city.name);
  selectedCity.value = null;
}
