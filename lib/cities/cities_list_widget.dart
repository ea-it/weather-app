import 'package:country_flags/country_flags.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../state_controller.dart';
import 'city.dart';

class CitiesList extends StatelessWidget {
  final ValueNotifier<City?> selectedCity;

  const CitiesList({
    super.key,
    required this.selectedCity,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      final stateController = StateController.to;
      return ListView.builder(
          itemCount: stateController.filteredCityList.length,
          itemBuilder: (context, i) {
            final currentCity = stateController.currentCity.value;
            final city = stateController.filteredCityList[i];
            final isCurrent = city.name == currentCity?.name;
            return ListTile(
              title: Text(city.name),
              selected: selectedCity.value?.name == city.name,
              leading: CountryFlag.fromCountryCode(
                city.countryCode,
                height: 20,
                width: 20,
                borderRadius: 4,
              ),
              trailing: isCurrent ? const Icon(Icons.check) : null,
              onTap: () {
                selectedCity.value = city;
              },
            );
          });
    });
  }
}
