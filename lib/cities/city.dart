class City {
  final String name;
  final double lat;
  final double lon;
  final String countryCode;

  const City({
    required this.name,
    required this.lat,
    required this.lon,
    required this.countryCode,
  });

  City.fromJson(Map data)
      : name = data['name'] as String,
        lat = data['lat'] as double,
        lon = data['lon'] as double,
        countryCode = data['country_code'] as String;
}
