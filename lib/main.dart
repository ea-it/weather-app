import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:get/get.dart';
import 'package:weather_app/cities/cities_screen.dart';
import 'package:weather_app/home/home_screen.dart';

import 'store_binding.dart';

void main() {
  runApp(const App());
}

class App extends HookWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    const title = 'Weather App';

    final index = useState(0);

    return GetMaterialApp(
      initialBinding: StoreBinding(),
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: _widgetOptions[index.value],
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.business),
              label: 'Cities',
            ),
          ],
          currentIndex: index.value,
          selectedItemColor: Colors.amber[800],
          onTap: (i) {
            index.value = i;
          },
        ),
      ),
    );
  }
}

final List<Widget> _widgetOptions = <Widget>[
  const HomeScreen(),
  const CitiesScreen(),
];
