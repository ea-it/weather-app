import 'package:get/get.dart';

import 'cities/city.dart';
import 'cities/load_cities.dart';
import 'weather/weather.dart';

class StateController extends GetxController {
  static StateController get to => Get.find();

  StateController() {
    _loadCities();
  }

  final cityList = RxList<City>([]);
  final filteredCityList = RxList<City>([]);
  final currentCity = Rx<City?>(null);
  final weather = Rx<Weather?>(null);
  final filter = Rx<String>('');

  void setCityList(List<City> cityList) {
    this.cityList.value = cityList;
    if (filter.value.isNotEmpty) {
      filteredCityList.value = _filterList(cityList, filter.value);
    } else {
      filteredCityList.value = cityList;
    }
  }

  void setCity(City city) {
    currentCity.value = city;
  }

  void setWeather(Weather? weather) {
    this.weather.value = weather;
  }

  void setFilter(String filter) {
    if (filter.isNotEmpty) {
      filteredCityList.value = _filterList(cityList, filter);
    } else {
      filteredCityList.value = cityList;
    }
    this.filter.value = filter;
  }
}

void _loadCities() async {
  final cities = await loadCities();
  cities.sort(
    (a, b) => a.name.compareTo(b.name),
  );
  StateController.to.setCityList(cities);
}

List<City> _filterList(List<City> list, String filter) => list
    .where((city) => city.name.toLowerCase().contains(filter.toLowerCase()))
    .toList();
