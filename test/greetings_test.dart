import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/home/greetings.dart';
import 'package:weather_app/home/util.dart';

void main() {
  test('Test greeting time in the morning', () {
    final now = DateTime(2023, 01, 01, 3);
    final greeting = prepareGreeting(now: now);

    expect(greeting, Greetings.morning);
  });

  test('Test greeting time at noon', () {
    final now = DateTime(2023, 01, 01, 12);
    final greeting = prepareGreeting(now: now);

    expect(greeting, Greetings.noon);
  });

  test('Test greeting time in the evening', () {
    final now = DateTime(2023, 01, 01, 18);
    final greeting = prepareGreeting(now: now);

    expect(greeting, Greetings.evening);
  });
}
